<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_web {
  public $ci;

  public function __construct() 
  {
    $this->ci =& get_instance();
    $this->ci->load->model('moption');
  }

  public function getOption($optionName)
  {
    $sql = $this->ci->moption->getOption($optionName);

    return $sql[0]['option_value'];
  }
}