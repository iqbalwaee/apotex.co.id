<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends FRONT_Controller {

	public function index()
	{
		$this->load->model('mpage');
		$page = $this->uri->segment(1);
		
		if($page == "about_us"){
			$id_page = 1;
		}elseif($page == "discount"){
			$id_page = 2;
		}elseif($page == "info_franchise"){
			$id_page = 3;
		}
		$this->data['page'] = $this->mpage->find('first',['id'=>$id_page]);
		$this->_render('pages/page');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */