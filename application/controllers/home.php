<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends FRONT_Controller {

	public function index()
	{
		$this->_render('pages/home');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */