<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends ADMIN_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model('muser');
	}

	public function index()
	{
		$this->load->model('muser');
		$data = $this->muser->find('all');
		$this->data['data'] = $data;
		$this->_render('backend/pages/user/index');
	}

	public function add()
	{
		$config = array(
			array(
					'field' => 'first_name',
					'label' => 'First Name',
					'rules' => 'trim|required|xss_clean'
			),
			array(
					'field' => 'last_name',
					'label' => 'Last Name',
					'rules' => 'trim|xss_clean'
			),
			array(
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'trim|required|xss_clean|valid_email|is_unique[users.email]'
			),
			array(
					'field' => 'username',
					'label' => 'Username',
					'rules' => 'trim|required|xss_clean|is_unique[users.username]'
			),
			array(
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'trim|required|xss_clean'
			),
			array(
					'field' => 'password2',
					'label' => 'Confrim Password',
					'rules' => 'trim|required|xss_clean|matches[password]'
			),
		);

		$this->form_validation->set_rules($config);
		
		if ($this->form_validation->run() == true)
		{
			$username = $this->input->post('username');
			$email    = $this->input->post('email');
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
			);
		}
		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data))
		{
			//check to see if we are creating the user
			//redirect them back to the admin page
			//$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("admin/user", 'refresh');
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$errors = $this->validation_errors_to_array($config);
    		$error 	=  current($errors);
			$this->data['message'] = ($error ? $error : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render('backend/pages/user/add');
		}
		
	}

	public function update($id)
	{
		$user = $this->ion_auth->user($id)->row();
		$this->data['user']= $user;
		$config = array(
			array(
					'field' => 'first_name',
					'label' => 'First Name',
					'rules' => 'trim|required|xss_clean'
			),
			array(
					'field' => 'last_name',
					'label' => 'Last Name',
					'rules' => 'trim|xss_clean'
			),
			array(
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'trim|required|xss_clean|valid_email|is_unique[users.email]'
			),
			array(
					'field' => 'old_email',
					'label' => 'Email',
					'rules' => 'trim|required|xss_clean|valid_email'
			),
			array(
					'field' => 'username',
					'label' => 'Username',
					'rules' => 'trim|required|xss_clean|is_unique[users.username]'
			),
			array(
					'field' => 'old_username',
					'label' => 'Username',
					'rules' => 'trim|required|xss_clean'
			),
			array(
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'trim|xss_clean'
			),
			array(
					'field' => 'password2',
					'label' => 'Confrim Password',
					'rules' => 'trim|xss_clean|matches[password]'
			),
		);
		$username = $this->input->post('username');
		$email    = $this->input->post('email');
		$old_username = $this->input->post('old_username');
		$old_email    = $this->input->post('old_email');
		if($email == $old_email){
			$config[2] = [
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'trim|required|xss_clean|valid_email'
			];
		}

		if($username == $old_username){
			$config[4] = [
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'trim|required|xss_clean'
			];
		}

		$this->form_validation->set_rules($config);
		
		if ($this->form_validation->run() == true)
		{
			$username = $this->input->post('username');
			$email    = $this->input->post('email');
			$password = $this->input->post('password');

			$data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
				'username' => $username,
				'email' => $email,
			);
			if(!empty($password)){
				$data['password'] = $password;
			}
		}
		if ($this->form_validation->run() == true && $this->ion_auth->update($user->id, $data))
		{
			//check to see if we are creating the user
			//redirect them back to the admin page
			//$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("admin/user", 'refresh');
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$errors = $this->validation_errors_to_array($config);
    		$error 	=  current($errors);
			$this->data['message'] = ($error ? $error : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->_render('backend/pages/user/edit');
		}
		
	}

	public function delete($id){
		$this->muser->delete($id);
		redirect(base_url('admin/user'));
	}

}

/* End of file user.php */
/* Location: ./application/controllers/backend/user.php */