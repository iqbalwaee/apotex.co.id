<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends ADMIN_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model('mpage');
	}

	public function index()
	{
		$this->load->model('mpage');
		$data = $this->mpage->find('all');
		$this->data['data'] = $data;
		$this->_render('backend/pages/page/index');
	}

	public function add()
	{
		$config = array(
			array(
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required|xss_clean'
			),
			array(
					'field' => 'content',
					'label' => 'Content',
					'rules' => 'trim|xss_clean'
			),
		);

		$this->form_validation->set_rules($config);
		
		if ($this->form_validation->run() == true)
		{
			$title = $this->input->post('title');
			$content    = $this->input->post('content');
			$status = $this->input->post('status');

			$data = array(
				'title' => $title,
				'content'  => $content,
				'status'  => $status,
			);
		}
		if ($this->form_validation->run() == true && $this->mpage->insert($data))
		{
			redirect("admin/page", 'refresh');
		}
		else
		{
			$errors = $this->validation_errors_to_array($config);
    		$error 	=  current($errors);
			$this->data['message'] = ($error ? $error : $this->session->flashdata('message'));
			$this->_render('backend/pages/page/add');
		}
		
	}

	public function update($id)
	{
		$this->data['page'] = $this->mpage->find('first',['id'=>$id]);
		$config = array(
			array(
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required|xss_clean'
			),
			array(
					'field' => 'content',
					'label' => 'Content',
					'rules' => 'trim|xss_clean'
			),
		);

		$this->form_validation->set_rules($config);
		
		if ($this->form_validation->run() == true)
		{
			$title = $this->input->post('title');
			$content    = $this->input->post('content');
			$status = $this->input->post('status');

			$data = array(
				'title' => $title,
				'content'  => $content,
				'status'  => $status,
			);
		}
		if ($this->form_validation->run() == true && $this->mpage->update($id,$data))
		{
			redirect("admin/page", 'refresh');
		}
		else
		{
			$errors = $this->validation_errors_to_array($config);
    		$error 	=  current($errors);
			$this->data['message'] = ($error ? $error : $this->session->flashdata('message'));
			$this->_render('backend/pages/page/edit');
		}
		
	}

	public function delete($id){
		$this->mpage->delete($id);
		redirect(base_url('admin/page'));
	}

}

/* End of file page.php */
/* Location: ./application/controllers/backend/page.php */