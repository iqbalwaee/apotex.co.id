<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends ADMIN_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
	}
	public function login()
	{
		$this->template = 'login_template';
		//validate form input
		$config = array(
			array(
					'field' => 'identity',
					'label' => 'Username / Email',
					'rules' => 'trim|required|xss_clean'
			),
			array(
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'trim|required|xss_clean'
			),
		);
		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == true)
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('admin/', 'refresh');

			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('admin/auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}

		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$errors = $this->validation_errors_to_array($config);
    		$error 	=  current($errors);
			$this->data['message'] = (!empty($error) ? $error : 'Sign in to start your session');
			$this->_render('backend/pages/login');
		}
		
		
	}

	//log the user out
	function logout()
	{
		$this->data['title'] = "Logout";

		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('admin/auth/login', 'refresh');
	}
}

/* End of file user.php */
/* Location: ./application/controllers/backend/user.php */