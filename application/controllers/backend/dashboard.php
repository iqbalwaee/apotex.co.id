<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends ADMIN_Controller {

	public function index()
	{
		$this->title = "Dashboard";
		$this->_render('backend/pages/dashboard');
	}
}

/* End of file user.php */
/* Location: ./application/controllers/backend/user.php */