<?php

class MY_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function validation_errors_to_array($validation_rules){
        $errors_array = [];
        foreach($validation_rules as $row){

            $field = $row['field'];         
            $error = form_error($field);    
            $error = strip_tags($error);
            if($error)
                $errors_array[] = $error;
         
        }

        return $errors_array;
    }
}

class FRONT_Controller extends MY_Controller 
{
    // Page info
    protected $data = array();
    protected $pageName = FALSE;
    protected $template = "main";
    protected $hasNav = TRUE;
    
    // Page contents 
    protected $javascript = array();
    protected $css = array();
    protected $fonts = array();
    
    // Page meta 
    protected $title = FALSE;
    protected $description = FALSE;
    protected $keywords = FALSE;
    protected $author = FALSE;
    protected $canonical_link = FALSE;
    protected $og_title = FALSE;
    protected $og_description = FALSE;
    protected $og_site_name = FALSE;
    protected $og_url = FALSE;
    protected $og_image = FALSE;
    protected $twitter_site = FALSE;
    protected $twitter_creator = FALSE;
    protected $twitter_title = FALSE;
    protected $twitter_desc = FALSE;


    // search form //
    protected $category = "All";
    protected $keywordsText = null;
    
    function __construct() 
    {
        parent::__construct();
        
        $this->title = $this->my_web->getOption('site_description');
        $this->description = $this->my_web->getOption('meta_description');
        $this->keywords = $this->my_web->getOption('meta_keywords');
        $this->author = 'global-tech.co.id';
        $this->canonical_link = $this->my_web->getOption('siteurl');
        $this->og_title = $this->my_web->getOption('site_description');
        $this->og_description = $this->my_web->getOption('meta_description');
        $this->og_site_name = $this->my_web->getOption('site_name');
        $this->og_url = $this->my_web->getOption('siteurl');
        $this->og_image = $this->my_web->getOption('siteurl').'/resources/img/logo.png';
        $this->twitter_site = $this->my_web->getOption('twitter');
        $this->twitter_creator = $this->my_web->getOption('twitter');
        $this->pageName = strToLower(get_class($this));
    }
    
    public function MY_Controller($load = array()) 
    {
        parent::__construct();
        
        foreach((array)$load as $k => $v) 
        {
            // explode files separated with +
            $v_array = explode('+', $v);
            
            // load each files
            foreach($v_array as $w) 
            {
                $this->load->$k(trim($w));
            }
        }
    } 
    
    protected function _render($view, $renderData = "FULLPAGE") 
    {
        switch($renderData) 
        {
            case "AJAX":
                $this->load->view($view, $this->data);
            break;
                
            case "JSON":
                echo json_encode($this->data);
            break;
            
            case "FULLPAGE":
            
            default:
                // static 
                $toTpl["javascript"] = $this->javascript;
                $toTpl["css"] = $this->css;
                $toTpl["fonts"] = $this->fonts;
                
                // meta
                $toTpl["title"] = $this->title;
                $toTpl["description"] = $this->description;
                $toTpl["keywords"] = $this->keywords;
                $toTpl["author"] = $this->author;
                $toTpl["canonical_link"] = $this->canonical_link;
                $toTpl["og_title"] = $this->og_title;
                $toTpl["og_description"] = $this->og_description;
                $toTpl["og_site_name"] = $this->og_site_name;
                $toTpl["og_url"] = $this->og_url;
                $toTpl["og_image"] = $this->og_image;
                $toTpl["twitter_site"] = $this->twitter_site;
                $toTpl["twitter_creator"] = $this->twitter_creator;
                $toTpl["twitter_title"] = $this->twitter_title;
                $toTpl["twitter_desc"] = $this->twitter_desc;
                
                // data
                $toBody["content_body"] = $this->load->view($view, array_merge($this->data, $toTpl), true);
                
                // nav menu 
                if($this->hasNav) 
                {
                    $this->load->helper("nav");
                    $toMenu["pageName"] = $this->pageName;
                    $toMenu["keywordsText"] = $this->keywordsText;
                    $toMenu["category"] = $this->category;
                    $toHeader["nav"] = $this->load->view("template/nav", $toMenu, true);
                }
                
                $toHeader["basejs"] = $this->load->view("template/basejs", $this->data, true);
                
                $toBody["header"] = $this->load->view("template/header", $toHeader, true);
                $toBody["footer"] = $this->load->view("template/footer", '', true);
                
                $toTpl["body"] = $this->load->view("template/" . $this->template, $toBody, true);
                
                // render view
                $this->load->view("template/main_template", $toTpl);
            break;
        }
    }
}

class ADMIN_Controller extends MY_Controller 
{
    // Page info
    protected $data = array();
    protected $pageName = FALSE;
    protected $template = "main";
    protected $hasNav = TRUE;
    
    // Page contents 
    protected $javascript = array();
    protected $css = array();
    protected $fonts = array();
    
    // Page meta 
    protected $title = FALSE;
    protected $description = FALSE;
    protected $keywords = FALSE;
    protected $author = FALSE;
    protected $canonical_link = FALSE;


    // User login info //
    protected $userLogin = null;
    
    function __construct() 
    {
        parent::__construct();
        $this->load->library(array('ion_auth'));
        $this->load->helper(array('language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $uri = $this->uri->segment(1);
        $uri .= '/'.$this->uri->segment(2);
        $uri .= '/'.$this->uri->segment(3);
        if (!$this->ion_auth->logged_in() && $uri != "admin/auth/login")
        {
            //redirect them to the login page
            redirect('admin/auth/login', 'refresh');
        }else{
            $users = $this->ion_auth->users()->result_array();
            $this->userLogin = $users;
        }
        $this->lang->load('auth');
        $this->title = $this->my_web->getOption('site_description');
        $this->description = $this->my_web->getOption('meta_description');
        $this->keywords = $this->my_web->getOption('meta_keywords');
        $this->author = 'global-tech.co.id';
        $this->canonical_link = $this->my_web->getOption('siteurl');
    }
    
    public function MY_Controller($load = array()) 
    {
        parent::__construct();
        
        foreach((array)$load as $k => $v) 
        {
            // explode files separated with +
            $v_array = explode('+', $v);
            
            // load each files
            foreach($v_array as $w) 
            {
                $this->load->$k(trim($w));
            }
        }
    } 
    
    protected function _render($view, $renderData = "FULLPAGE") 
    {
        switch($renderData) 
        {
            case "AJAX":
                $this->load->view($view, $this->data);
            break;
                
            case "JSON":
                echo json_encode($this->data);
            break;
            
            case "FULLPAGE":
            
            default:
                // static 
                $toTpl["javascript"] = $this->javascript;
                $toTpl["css"] = $this->css;
                $toTpl["fonts"] = $this->fonts;
                
                // meta
                $toTpl["title"] = $this->title;
                $toTpl["description"] = $this->description;
                $toTpl["keywords"] = $this->keywords;
                $toTpl["author"] = $this->author;
                // data
                if($this->template != "login_template"){
                    $toBody["content_body"] = $this->load->view($view, array_merge($this->data, $toTpl), true);
                     // nav menu 
                    if($this->hasNav) 
                    {
                        $this->load->helper("nav");
                        $toMenu["pageName"] = $this->pageName;
                        $toMenu["userLogin"] = $this->userLogin[0];
                        $toHeader["nav"] = $this->load->view("backend/template/nav", $toMenu, true);
                    }
                    
                    
                    $toBody["header"] = $this->load->view("backend/template/header", $toHeader, true);
                    $toBody["sidebar"] = $this->load->view("backend/template/sidebar", '', true);
                    $toBody["footer"] = $this->load->view("backend/template/footer", '', true);
                    $toTpl["body"] = $this->load->view("backend/template/" . $this->template, $toBody, true);
                    $this->load->view("backend/template/main_template", $toTpl);
                }else{
                    $toBody = [];
                    $toTpl["body"] = $this->load->view($view,array_merge($this->data, $toTpl),true);
                    $this->load->view("backend/template/".$this->template, $toTpl);
                }
               
                
                
                
                // render view
                
            break;
        }
    }
}