<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');

class Moption extends CI_Model {
	private $TBL_OPTION = 'options';

	public function __construct() 
	{
		parent::__construct();
	}
	
	public function getOptions() 
	{
		$sql = $this->db->get($this->TBL_OPTION);
		
		return array(
			'total'		=> $sql->num_rows(),
			'row'		=> $sql->result_array()
		);
	}

	public function getOption($optionName) 
	{
		$sql = $this->db->where('option_name', $optionName)
					->get($this->TBL_OPTION);
					
		return $sql->result_array();
	}
	
	public function updateSetting($optionName, $data) 
	{
		$this->db->where('option_name', $optionName);
		
		return $this->db->update($this->TBL_OPTION, $data);
	}	
}

/* End of file moption.php */
/* Location: ./application/controllers/moption.php */