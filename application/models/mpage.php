<?php if(!defined('BASEPATH')) exit ('No direct script access allowed');

class Mpage extends CI_Model {
	private $TBL_PAGE = 'pages';

	public function __construct() 
	{
		parent::__construct();
	}

	public function find($type='all',$conditions=null)
	{
		if( $conditions != null){
			$this->db->where($conditions);
		}

		$sql = $this->db->get($this->TBL_PAGE);
		if($type == "all"){
			return $sql->result_array();
		}elseif($type == "first"){
			$data = $sql->result_array();
			$count = count($data);
			if($count != 0){
				return $data[0];
			}else{
				return array();	
			} 
		}elseif($type == "count"){
			$data = $sql->result_array();
			$count = count($data);
			return $count;
		}
	}

	public function insert($data) {		
		return $this->db->insert($this->TBL_PAGE, $data);
	}

    public function update($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update($this->TBL_PAGE, $data);
    }

    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete($this->TBL_PAGE);
    }
		
}

/* End of file moption.php */
/* Location: ./application/controllers/moption.php */