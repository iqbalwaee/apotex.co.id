<!--  Navbar - START -->
<nav class="navbar navbar-defaulthead" role="navigation">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-4">
         <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <div class="logo"> 
              <a href="<?=base_url();?>"> <img src="<?= base_url(IMAGES.'/') ?>/logoapotik.png" alt="..."> </a>
            </div>
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
        </div>
      </div>
      <div class="col-md-5 col-sm-5 topsearchhor">        
          <form class="horizontal2" role="search" style="padding:0px;">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search" name="q" style="height:38px;">
                <div class="input-group-btn">
                  <button class="btn2 btn-success1" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
          </form>
        <div id="navbarCollapse" class="collapse navbar-collapse topmen1">    
          <div class="topmen1">
          <ul class="nav navbar-nav">
              <li class="active"><a href="#">HOME</a></li>
              <li><a href="#">ABOUT US</a></li>
              <li><a href="#">OUTLETS</a></li>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">SERVICES <b class="caret"></b></a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="services.php">DOWNLOAD BROSUR</a></li>
                    <li><a href="services.php">DOWNLOAD BROSUR 2</a></li>
                    <li><a href="services.php">DOWNLOAD BROSUR 3</a></li>
                  </ul>
              </li>
              <li><a href="discount.php">DISCOUNT</a></li>
              <li><a href="info_franchise.php">INFO FRANCHISE</a></li>
              <li><a href="contact.php">CONTACT US</a></li>
          </ul>  
          </div>        
        </div>
      </div>
      <div class="col-md-4 col-sm-3">
        <div class="social pull-right">
          <a href="http://facebook.com" target="_blank"><img src="<?= base_url(IMAGES.'/') ?>/fb.png"></a>
          <a href="http://twitter.com" target="_blank"><img src="<?= base_url(IMAGES.'/') ?>/twitter.png"></a>
          <a href="http://instagram.com" target="_blank"><img src="<?= base_url(IMAGES.'/') ?>/instagram.png"></a>
          <a href="https://plus.google.com" target="_blank"><img src="<?= base_url(IMAGES.'/') ?>/google.png"></a>
          <a href="#myModal" id="0" class="btn2 btn-success1" data-toggle="modal">
            <i class="icon-plus"></i> LOGIN
          </a>
        </div>
      </div>

      <div class="col-sm-12 col-md-9 topmen">  
            <ul class="nav navbarmenu">
              <li><a href="<?=base_url();?>">HOME</a></li>
              <li style="padding-top:15px;"> | </li>
              <li><a href="<?=base_url('about_us');?>">ABOUT US</a></li>
              <li style="padding-top:15px;"> | </li>
              <li><a href="<?=base_url('outlet');?>">OUTLETS</a></li>
              <li style="padding-top:15px;"> | </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">SERVICES <b class="caret"></b></a>
                <ul class="dropdown-menuap">
                  <li style="margin-right: 0px;"><a href="<?=base_url('services');?>">DOWNLOAD BROSUR</a></li>
                  <li style="margin-right: 0px;"><a href="<?=base_url('services');?>">DOWNLOAD BROSUR 2</a></li>
                  <li style="margin-right: 0px;"><a href="<?=base_url('services');?>">DOWNLOAD BROSUR 3</a></li>
                </ul>
              </li>
              <li style="padding-top:15px;"> | </li>
              <li><a href="<?=base_url('discount');?>">DISCOUNT</a></li>
              <li style="padding-top:15px;"> | </li>
              <li><a href="<?=base_url('info_franchise');?>">INFO FRANCHISE</a></li>
              <li style="padding-top:15px;"> | </li>
              <li><a href="<?=base_url('contact_us');?>">CONTACT US</a></li>
            </ul>
        </div>
      
    </div>
  </div>  
</nav>
<!-- Search Navbar - END -->
