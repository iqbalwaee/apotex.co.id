<footer id="footer" class="footer-distributed">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <span>About ApoteX</span>
                <p>Badan hukum   :    PT. Prima Farma Indonesia </p>
                <p>Direktur / Founder  :    Ir. Thomas Lie, MM</p>
                <p>Yang juga adalah pendiri Grup MY SALON (saat ini berjumlah 80 cabang, di seluruh Indonesia). </p>
                <p><a href="about.php">Read More..</a></p>
              <span style="padding-top:10px;">Head Office &nbsp;</span>
              <p>
                Jl Raya Arjuna No.65
                Surabaya - Jawa Timur
                031-546-2345
              </p>
            </div>
            <div class="col-sm-12 col-md-3">
              <span>General Services</span>
              <div class="footer-links">
                 <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="about.php">ABOUT US</a></li>
                    <li><a href="outlet.php">OUTLETS</a></li>
                    <li><a href="services.php">SERVICES</a></li>
                    <li><a href="discount.php">DISCOUNT</a></li>
                    <li><a href="info_franchise.php">INFO FRANCHISE</a></li>
                    <li><a href="contact.php">CONTACT US</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
            <span>Recent Post</span>
                <div class="footer-linksre">
                 <ul>
                    <li><a href="#">New Franchise in BOGOR</a></li>
                    <li><a href="#">Global and International SERVICES</a></li>
                    <li><a href="#">New Franchise in PAPUA</a></li>
                    <li><a href="#">Fast, economical shipping services</a></li>
                    <li><a href="#">Our service strengths</a></li>
                </ul>
                </div>
            </div>

            <div class="col-sm-12 col-md-3">
            <span>Subscribe!</span>
            <div class="footer-right">
              <form role="form">
                <div class="form-group">
                  <label for="name">NAME</label>
                  <input type="text" class="form-control" id="name" placeholder="Name..">
                </div>
                <div class="form-group">
                  <label for="email">EMAIL</label>
                  <input type="email" class="form-control" id="email" placeholder="Email..">
                </div>
                
                <button type="submit" class="btn2foot btn-success1foot">Submit</button>
              </form>
  <!--
                  <form action="#" method="post">
                    <label for="exampleInputEmail1" class="emailname">NAME</label>
                      <input type="text" class="form-control" placeholder="Name..">
                    <label for="exampleInputEmail1" class="emailname">EMAIL</label>
                      <input type="text" class="form-control" placeholder="Email..">
                    <button type="submit" class="btn2foot btn-success1foot">Submit</button>
                  </form>
                  <!--
                  <div class="footer-icons">
                    <div>
                      <span>Keep in Touch</span>
                    </div>
                      <ul class="nav navbar-left">
                        <a href="http://facebook.com" target="_blank"><img src="image/fb.png"></a>
                        <a href="http://twitter.com" target="_blank"><img src="image/twitter.png"></a>
                        <a href="http://instagram.com" target="_blank"><img src="image/instagram.png"></a>
                        <a href="https://plus.google.com" target="_blank"><img src="image/google.png"></a> 
                      </ul>
                  </div>
                -->
              </div>
            </div>
        </div>

    </div>   
</footer><!--/#footer-->

 <div class="col-md-12" style="background-color:#2A2A2A;color:#FFF;font-family:Arial;">
      <div class="row">
        <div class="col-xs-12 col-sm-12 copyright">
         <p>&copy; 2015 Global Technology Resources. All Rights Reserved.</p>
        </div>
      </div>
  </div>
