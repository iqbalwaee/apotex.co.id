<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, height=device-height, initial scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="title" content="<?php echo $title ?>" />
<meta name="description" content="<?php echo $description ?>" />
<meta name="keywords" content="<?php echo $keywords ?>" />
<meta name="author" content="<?php echo $author ?>" />
<link rel="shortcut icon" href="<?= base_url(IMAGES) ?>/logoapotik.png"/>
<link rel="stylesheet" href="<?= base_url(CSS) ?>/bootstrap.min.css" />
<link rel="stylesheet" href="<?= base_url(CSS) ?>/bootstrap-theme.min.css" />

<!--Custom CSS -->
<link rel="stylesheet" href="<?= base_url(CSS) ?>/header.css">
<link rel="stylesheet" href="<?= base_url(CSS) ?>/homepage.css" />
<link rel="stylesheet" href="<?= base_url(CSS) ?>/footer.css">
<!--[if lt IE 9]>
 <script src=”https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js”></script>
   <script src=”https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js”></script>
<![endif]-->    
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title ?></title>
</head>
<body>
<?php echo $body ?>      
</body>
</html>
<script src="<?= base_url(JS) ?>/jquery.min.js"></script>
<script src="<?= base_url(JS) ?>/bootstrap.min.js"></script>