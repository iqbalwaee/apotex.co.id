<div class="container-fluid">
    <div class="row">
   <div class="col-md-12" style="padding:0px;">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
      
        <!-- Wrapper for slides -->
        <div class="carousel-inner topcarousel">
          <div class="item active">
            <img src="<?= base_url(IMAGES.'/') ?>/banner1.jpg" width="100%" alt="...">
            <div class="carousel-caption captionslide">
              <h1>Apotex Services that you can trust</h1>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </div>
          </div>
          <div class="item">
            <img src="<?= base_url(IMAGES.'/') ?>/banner2.jpg" width="100%" alt="...">
            <div class="carousel-caption captionslide">
              <h1>Apotex Services that you can trust</h1>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </div>
          </div>
          <div class="item">
            <img src="<?= base_url(IMAGES.'/') ?>/banner3.jpg" width="100%" alt="...">
            <div class="carousel-caption captionslide">
              <h1>Apotex Services that you can trust</h1>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </div>
          </div>          
        </div>

        <!-- Controls -->
        <div class="container">
          <div class="row">
            <a class="left carousel-controlhome" href="#carousel-example-generic" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left chevron-besar"></span>
            </a>
            <a class="right carousel-controlhome" href="#carousel-example-generic" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right chevron-besar"></span>
            </a>
          </div>
        </div>
      </div><!-- /.carousel-example-generic -->    
    </div><!-- /.col-md-6 -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
<!--infolates -->
<div class="container">
  <div class="row">
    <div class="col-md-12">
        <div class="img-responsive imglates">         
              <img src="<?= base_url(IMAGES.'/') ?>/info_franchise.png" width="100%" alt="...">
        </div>   
    </div>
  </div>
  <div class="row">
      <div class="col-sm-6 col-md-8">
        <div class="lates">
          Latest News from <b>ApoteX</b>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
          <a href="latestnews.php" class="viewlates"> View all latest News >> </a>
      </div>
  </div>
</div>
  <!-- end info lates -->

<div class="container">
  <div class="row">
    <div class="slick">

      <div class="col-sm-6 col-md-4">
        <div class="thumbnail thumblabel">
          <img src="<?= base_url(IMAGES.'/') ?>/news1.png" width="100%">
            <div class="caption">
              <h4><strong>New Franchise in BOGOR</strong></h4>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            </div>          
        </div>
        <div class="center1">
          <p><a href="#"><button type="button" class="btn2 btn-success1">Read More</button></a></p>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="thumbnail thumblabel">
        <img src="<?= base_url(IMAGES.'/') ?>/news2.png" width="100%">
            <div class="caption">
              <h4><strong>Global and International SERVICES</strong></h4>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            </div>          
        </div>
        <div class="center1">
          <p><a href="#"><button type="button" class="btn2 btn-success1">Read More</button></a></p>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="thumbnail thumblabel">
          <img src="<?= base_url(IMAGES.'/') ?>/news3.png" width="100%">
            <div class="caption">
              <h4><strong>New Franchise in PAPUA</strong></h4>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            </div>            
        </div>
        <div class="center1">
          <p><a href="#"><button type="button" class="btn2 btn-success1">Read More</button></a></p>
        </div>     
      </div> 
    </div>   <!-- slick -->
  </div>
</div><!-- /.container -->