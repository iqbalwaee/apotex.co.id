<header class="main-header">

  <!-- Logo -->
  <a href="<?=base_url('admin');?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>APTX</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>APOTEX</b> Admin Panel</span>
  </a>

  <?php echo $nav ?>
</header>