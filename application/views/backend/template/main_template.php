<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?=$title;?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?=base_url(BACKEND_SOURCE.'/bootstrap');?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?=base_url(BACKEND_SOURCE.'/plugins');?>/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=base_url(BACKEND_SOURCE.'/plugins');?>/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url(BACKEND_SOURCE.'/dist');?>/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?=base_url(BACKEND_SOURCE.'/dist');?>/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?=base_url(BACKEND_SOURCE.'/plugins');?>/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <meta name="title" content="<?php echo $title ?>" />
    <meta name="description" content="<?php echo $description ?>" />
    <meta name="keywords" content="<?php echo $keywords ?>" />
    <meta name="author" content="<?php echo $author ?>" />
    <link rel="shortcut icon" href="<?= base_url(IMAGES) ?>/logoapotik.png"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-red-light sidebar-mini">
    <?php echo $body ?> 

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?=base_url(BACKEND_SOURCE.'/bootstrap');?>/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?=base_url(BACKEND_SOURCE.'/plugins');?>/fastclick/fastclick.min.js'></script>

    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url(BACKEND_SOURCE.'/dist');?>/js/app.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/chartjs/Chart.min.js" type="text/javascript"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?=base_url(BACKEND_SOURCE.'/dist');?>/js/pages/dashboard2.js" type="text/javascript"></script>
    <script src="<?=base_url(BACKEND_SOURCE.'/plugins');?>/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url(BACKEND_SOURCE.'/dist');?>/js/global.js" type="text/javascript"></script>
  </body>
</html>