<section class="content-header">
  <h1>
    Users
    <small>Management data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Users</a></li>
    <li class="active">Add new user</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Add New User</h3>
          <a href="<?=base_url('admin/user/');?>" class="btn btn-info btn-flat btn-sm pull-right"><i class="fa fa-chevron-left"></i> Data User</a>
          <div class="clearfix"></div>
        </div><!-- /.box-header -->
        <div class="box-body">
        	<?php if(!empty($message)):?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-ban"></i> Alert!</h4>
					<?=$message;?>
				</div>
        	<?php endif;?>
        	<form action="<?=base_url('admin/user/add');?>" method="post">
        	<div class="form-horizontal">
        		<div class="form-group">
        			<label class="control-label col-lg-3 col-md-4">First Name</label>
        			<div class="col-lg-4 col-md-6">
        				<input type="text" value="<?=set_value('first_name');?>" name="first_name" class="form-control">
        			</div>
        		</div>
        		<div class="form-group">
        			<label class="control-label col-lg-3 col-md-4">Last Name</label>
        			<div class="col-lg-4 col-md-6">
        				<input type="text" value="<?=set_value('last_name');?>" name="last_name" class="form-control">
        			</div>
        		</div>
        		<div class="form-group">
        			<label class="control-label col-lg-3 col-md-4">Email</label>
        			<div class="col-lg-4 col-md-6">
        				<input type="text" value="<?=set_value('email');?>" name="email" class="form-control">
        			</div>
        		</div>
        		<div class="form-group">
        			<label class="control-label col-lg-3 col-md-4">Username</label>
        			<div class="col-lg-4 col-md-6">
        				<input type="text" value="<?=set_value('username');?>" name="username" class="form-control">
        			</div>
        		</div>
        		<div class="form-group">
        			<label class="control-label col-lg-3 col-md-4">Password</label>
        			<div class="col-lg-4 col-md-6">
        				<input type="password" value="<?=set_value('password');?>" name="password" class="form-control">
        			</div>
        		</div>
        		<div class="form-group">
        			<label class="control-label col-lg-3 col-md-4">Confirm Password</label>
        			<div class="col-lg-4 col-md-6">
        				<input type="password" value="<?=set_value('password2');?>" name="password2" class="form-control">
        			</div>
        		</div>
        		<div class="form-group">
        			<div class="col-lg-offset-3 col-md-offset-4 col-lg-4 col-md-6">
        				<button class="btn btn-success" type="submit">Submit</button>
        			</div>
        		</div>
        	</div>
        	</form>
        </div>
      </div>
    </div>
  </div>
</section>