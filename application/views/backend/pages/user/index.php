<section class="content-header">
  <h1>
    Users
    <small>Management data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Users</a></li>
    <li class="active">Data users</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Users</h3>
          <a href="<?=base_url('admin/user/add');?>" class="btn btn-info btn-flat btn-sm pull-right"><i class="fa fa-plus"></i> Add New User</a>
          <div class="clearfix"></div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="dataTable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Username</th>
                <th>Email</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th width="120px;">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data as $key => $r):?>
                <tr>
                  <td><?=$r['username'];?></td>
                  <td><?=$r['email'];?></td>
                  <td><?=$r['first_name'];?></td>
                  <td><?=$r['last_name'];?></td>
                  <td class="text-center">
                    <div class="btn-group btn-group-sm btn-group-flat">
                       <a href="<?=base_url('admin/user/update/'.$r['id']);?>" class="btn bg-purple btn-flat"><i class="fa fa-pencil"></i> Edit</a>
                       <a href="<?=base_url('admin/user/delete/'.$r['id']);?>" class="btn bg-maroon btn-flat"><i class="fa fa-trash-o"></i> Delete</a>
                    </div>
                  </td>
                </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>