<section class="content-header">
  <h1>
    Pages
    <small>Management data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Pages</a></li>
    <li class="active">Add new page</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Add New Page</h3>
          <a href="<?=base_url('admin/page/');?>" class="btn btn-info btn-flat btn-sm pull-right"><i class="fa fa-chevron-left"></i> Data Pages</a>
          <div class="clearfix"></div>
        </div><!-- /.box-header -->
        <div class="box-body">
        	<?php if(!empty($message)):?>
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-ban"></i> Alert!</h4>
					<?=$message;?>
				</div>
        	<?php endif;?>
        	<form action="<?=base_url('admin/page/add');?>" method="post">
        	<div class="form-horizontal">
        		<div class="form-group">
        			<label class="control-label col-lg-3 col-md-4">Title</label>
        			<div class="col-lg-4 col-md-6">
        				<input type="text" value="<?=set_value('title');?>" name="title" class="form-control">
        			</div>
        		</div>
        		<div class="form-group">
        			<label class="control-label col-lg-3 col-md-4">Content</label>
        			<div class="col-lg-9 col-md-8">
        				<textarea name="content" class="textarea-custom" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=set_value('content');?></textarea>
        			</div>
        		</div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-4">Status</label>
                    <div class="col-lg-4 col-md-6">
                        <select name="status" class="form-control" id="status">
                            <option value="0">Inactive</option>
                            <option value="1">Active</option>
                        </select>
                    </div>
                </div>
        		<div class="form-group">
        			<div class="col-lg-offset-3 col-md-offset-4 col-lg-4 col-md-6">
        				<button class="btn btn-success" type="submit">Submit</button>
        			</div>
        		</div>
        	</div>
        	</form>
        </div>
      </div>
    </div>
  </div>
</section>