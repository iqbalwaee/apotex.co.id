<section class="content-header">
  <h1>
    Pages
    <small>Management data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Users</a></li>
    <li class="active">Data users</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Data Page</h3>
          <a href="<?=base_url('admin/page/add');?>" class="btn btn-info btn-flat btn-sm pull-right"><i class="fa fa-plus"></i> Add New Page</a>
          <div class="clearfix"></div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="dataTable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th width="150px;">Title</th>
                <th>Content</th>
                <th>Status</th>
                <th width="120px;">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data as $key => $r):?>
                <tr>
                  <td><?=$r['title'];?></td>
                  <td>
                    <div class="slimScroll">
                      <?=$r['content'];?>
                    </div>
                  </td>
                  <td>
                    <?php 
                      if($r['status']){
                        echo "<label class='label bg-blue'>Active</label>";
                      }else{
                        echo "<label class='label bg-orange'>Inactive</label>";
                      }
                    ?>
                  </td>
                  <td class="text-center">
                    <div class="btn-group btn-group-sm btn-group-flat">
                       <a href="<?=base_url('admin/page/update/'.$r['id']);?>" class="btn bg-purple btn-flat"><i class="fa fa-pencil"></i> Edit</a>
                       <a href="<?=base_url('admin/page/delete/'.$r['id']);?>" class="btn bg-maroon btn-flat"><i class="fa fa-trash-o"></i> Delete</a>
                    </div>
                  </td>
                </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>