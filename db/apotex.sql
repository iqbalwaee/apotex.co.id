-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2015 at 11:17 
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `apotex`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
`id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` char(100) DEFAULT NULL,
  `description` text,
  `picture` char(200) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id_option` int(11) NOT NULL,
  `option_name` varchar(64) DEFAULT NULL,
  `option_value` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id_option`, `option_name`, `option_value`) VALUES
(3, 'site_description', 'Apotex.co.id'),
(2, 'site_name', 'APOTEX'),
(1, 'siteurl', 'http://apotex.co.id'),
(4, 'admin_email', 'admin@apotex.co.id'),
(5, 'order_email', 'order@apotex.co.id'),
(6, 'support_email', 'support@apotex.co.id'),
(7, 'request_email', 'request@apotex.co.id'),
(8, 'info_email', 'info@apotex.co.id'),
(9, 'help_email', 'help@apotex.co.id'),
(10, 'purchasing_email', 'purchasing@apotex.co.id'),
(11, 'smtp1_host', 'ssl://smtpout.asia.secureserver.net'),
(12, 'smtp1_port', '465'),
(13, 'smtp1_user', 'order@apotex.co.id'),
(14, 'smtp1_password', 'hgdunia123'),
(15, 'smtp2_host', 'ssl://smtpout.asia.secureserver.net'),
(16, 'smtp2_port', '465'),
(17, 'smtp2_user', 'request@apotex.co.id'),
(18, 'smtp2_password', 'hgdunia123'),
(19, 'meta_keywords', 'amazon, ebay, walmart, shopbop, Lego superheroes,  lego friends, lego brick, lego chima, lego builder, lego city, lego starwars, lego captain america, lego hulk, lego spiderman, coach bag, tumi bag, michael kors, casio gshock, g-shock, motorola moto g, moto x, lenovo, dell venue, nexus, google, android, kate spade, timex watch, chromecast, miracast, apple tv, disney, frozen, elsa, olaf, cars, thor, plane, lowest price, murah, termurah, jakarta, indonesia, surabaya, bandung, medan, makassar, banjarmasin, kalimantan, malang, semarang, yogyakarta, mainan, baju, juice, smartphone, tablet, case, moto shell, vitamin, omega 3, vitamin c, calcium, dha, cheetos, flamin hot, pedas, susu import, sepeda, mountain bike, gucci, prada, dompet, necklace, jam, seiko, mega blok, lego architecture, lego minecraft, barbie, mattel, hasbro, fisher price, furby boom, furbling, furby, soccer, world cup, berf, transformer, hotwheel, hot wheel, hobby, remote control, tamiya, tomica, tomi, thomas train, tomy, fire tv, kindle, star wars, hello kitty, pokemon, lalaloopsy, ironman, lego duplo, lego baby, lego atlantis, lego aquazone, lego bionicle, lego dino attack, lego hero factory, lego kingdoms, lego mindstorms, belanja, online, shopping, murah, amazon, shopbop, pakaian, baju, makanan, michael, kors, tumi, prada, nike, adidas, victoria, secret, bag, tas, avenger, lego, mainan, toy, shirt, golf, music, book'),
(20, 'meta_description', 'apotex.co.id adalah toko online termurah di Indonesia dimana anda bisa membeli produk luar negri dengan murah seperti berbelanja di Amazon.com'),
(21, 'contact_us', '<p>apotex.co.id</p>\r\n<p>\r\nJalan Gading Kirana <br>\r\nRukan Gading Bukit Indah Blok J no. 1 <br>\r\nJakarta Utara 14240 <br>\r\nPhone : +6221-45856676 <br>\r\nFax : +6221-45856677 <br>\r\nEmail : help@apotex.co.id <br>\r\n</p>'),
(22, 'pin_bb', '217CD880'),
(23, 'instagram_link', 'http://instagram.com/hargadunia?ref=badge'),
(24, 'site_footer', 'apotex.co.id '),
(26, 'invoice_contact_footer', '<p>apotex.co.id <br>\r\nJalan Gading Kirana <br>\r\nRukan Gading Bukit Indah Blok J no. 1 <br>\r\nJakarta Utara 14240 <br>\r\nEmail : help@apotex.co.id <br>\r\nPhone : +6221-45856676 <br>\r\n</p>\r\n'),
(27, 'inputsearch_words', 'Pencarian dari 300.000 produk'),
(28, 'affiliate', 'http://conecworld.com/beta'),
(29, 'cs_mobile_phone', '+6281314176876'),
(30, 'cw_key_id', 'demoapp'),
(31, 'cw_secret_id', 'demopass'),
(32, 'twitter', '@apotex');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
`id` int(11) NOT NULL,
  `title` char(50) DEFAULT NULL,
  `content` text,
  `status` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `status`, `created`, `modified`) VALUES
(1, 'About Us', '<b>Mengenai ApoteX\n            </b><div>\n              <p>Badan hukum   :    PT. Prima Farma Indonesia </p>\n              <p>Direktur / Founder  :    Ir. Thomas Lie, MM</p>\n              <p>Yang juga adalah pendiri Grup MY SALON (saat ini berjumlah 80 cabang, di seluruh Indonesia). </p>\n              <p>Terdaftar di :  </p>\n              <p>Asosiasi Franchise Indonesia (AFI).</p> \n              <p>Perhimpunan Waralaba dan     Lisensi Indonesia (WALI). </p>\n              <p>Mulai Aktif  :   Outlet pertama beroperasi Januari 2012 di Jalan Ngageljaya, Surabaya. </p>  \n              <p>Jumlah Cabang  : Saat ini telah membuka 21 cabang.</p>\n            </div>\n            <h4>Visi dan Misi ApoteX </h4>\n              <div>\n              <p>Bagi Karyawan : Menjadikan ApoteX tempat bekerja yang \nnyaman, aman, dan berkesinambungan; dan memperoleh penghasilan yang \nlebih baik. </p>\n              <p>Bagi Pelanggan / Pasien : Menjadikan ApoteX sebagai \napotek pilihan untuk kebutuhan obat resep, obat bebas (OTC), dan food \nsupplement dengan harga murah. </p>\n              <p>Bagi Franchisee : Menjadikan ApoteX sebagai sebuah sarana investasi yang menguntungkan dan dapat dibanggakan. </p>\n              </div>\n            <h4>Peranan PT. Prima Farma Indonesia (PT. PFI) selaku franchisor </h4> \n              <div> \n              <p>Melakukan survey dan menyetujui lokasi bakal calon ApoteX. </p> \n              <p>Menyiapkan dan menjelaskan   Standard  Operating  Procedure (SOP) kepada calon franchisee.</p>\n              <p>Merekrut tenaga kerja apoteker dan asisten apoteker. </p>\n              <p>Memastikan bahwa semua tenaga kerja apotek memenuhi standar kualitas ApoteX. </p>\n              <p>Mengurus dan menyelesaikan Surat Ijin Apotek (SIA) setempat.</p>\n              <p>Menunjuk rekanan kontraktor PT. PFI untuk melakukan pekerjaan renovasi, \n                 pembuatan furniture, dan signage sesuai dengan standard ApoteX.</p> \n              <p>Menerbitkan Laporan Neraca, Laba Rugi, dan Arus Kas secara berkala. -  Berdasarkan surat kuasa dari franchisee, \n                 menjalankan operasional ApoteX dengan sungguh - sungguh dan dengan memperhatikan prinsip prudent.</p>\n              <p>Melakukan pembayaran (pay out) laba hasil usaha kepada Franchisee pada bulan ketujuh dan setiap tiga bulan setelah itu. </p>  \n              </div>\n            <h4>Peranan Franchisee </h4>\n              <div> \n              <p>Menyediakan dana investasi : Franchise Fee Rp 55 juta, renovasi + peralatan sekitar Rp 250 juta\n                 dan pembiayaan pembelian obat awal sekitar Rp 250 juta. </p>\n              <p>Bersedia membeli obat awal dan lanjutan dari franchisor (PT. PFI).</p>\n              <p>Memberi Kuasa kepada PT. PFI untuk mengelola operasional, administrasi, dan keuangan Kegiatan Usaha.</p>\n              <p>Merahasiakan informasi-informasi dan pengetahuan mengenai Kegiatan Usaha, \n                 tanda Hak Milik Intelektual atau Sistem Usaha dan turut mengusahakan agar karyawan \n                 dan staffnya dapat menjaga kerahasiaan informasi dan pengetahuan tersebut kepada pihak lain.</p>\n              </div>\n            <h4>Time Frame </h4>\n              <div> \n              <p>Hari H   : sign Perjanjian Waralaba.</p>\n              <p>Hari H   : bayar franchise fee (minus biaya survey, bila ada).</p>\n              <p>Hari H + 7   : franchisee menyetujui lay out ruangan dan RAB.</p>\n              <p>Hari H + 7 : franchisee bayar 50% pertama dari total nilai RAB.</p>\n              <p>Hari H + 14  : pekerjaan renovasi dan fitting out dimulai. </p> \n              <p>Hari H + 40  : franchisee bayar 50% kedua dari total nilai RAB.</p>\n              <p>Hari H + 44  : pekerjaan renovasi dan fitting out selesai + Serah Terima Hari</p>  \n              <p>H + 45   : settlement RAB (bayar selisih lebih / kurang nilai pekerjaan renovasi).</p>\n              <p>Hari H + 45  : OPENING.</p>\n              </div>\n            <h4>Kebijakan HRD ApoteX</h4>\n              <div> \n              <p>Penerimaan karyawan baru haruslah atas persetujuan dan ditempatkan oleh PT. PFI.</p>\n              <p>PT. PFI dapat melakukan penambahan, mutasi, dan pemecatan karyawan, atas persetujuan franchisee. </p>\n                <p> - Gaji Pokok Apoteker Penanggung Jawab Apotek (APA) = Rp 3 juta; </p>\n                 <p>  Apoteker Pendamping = Rp 2.5 juta; </p>\n                 <p> Asisten Apoteker = Rp 1.9 juta.</p>\n              <p>Seluruh karyawan berhak atas Uang Racik (uang R) sebesar 1% dari nilai resep; \n                 yang pembagian di antara karyawan nya diatur oleh PT. PFI. </p>\n              <p>Seluruh karyawan berhak atas bonus omzet sebesar 1%, setelah mencapai omzet Rp 250 juta per bulan nya.</p>\n              <p>PPH 21 dibayarkan oleh Kegiatan Usaha, dan dibebankan kepada masing - masing karyawan.</p> \n              <p>Di outlet ApoteX tidak ada kasir, staff pembelian, staff pembayaran, staff administrasi, ataupun staff keuangan.  \n                 Franchisee berhemat banyak dari penghapusan pos jabatan - pos jabatan ini. </p>\n              <p>PT. PFI menyelenggarakan Koperasi Simpan Pinjam dan Asuransi Kesehatan bagi seluruh karyawan ApoteX. </p>\n              </div>\n            <h4>Kebijakan Operasional </h4>\n              <div> \n              <p>Jam buka - tutup : Disesuaikan dengan kondisi setempat. \n                 Secara umum Apotex beroperasi pada pukul 07.00 – 23.00 waktu setempat.</p>\n              <p>Shift 1 : 07.00 – 15.00 ; Shift 2 : 15.00 – 23.00 ; Shift 3 : 12.00 – 20.00</p>\n              <p>Seluruh karyawan memakai Jas Lab. - Pasien ditangani oleh satu orang Apoteker / Asisten Apoteker mulai dari penerimaan resep, \n                 perkiraan harga, peracikan, penyerahan obat, hingga pembayaran (close-loop system). \n                 Lebih cepat, efisien, dan menghindari kesalahan racik</p>\n              <p>Melayani permintaan Obat Generik.</p>\n              <p>Transaksi Online ; cabang - kantor pusat.</p> \n              </div>\n             <h4>Kebijakan Pembelian</h4>\n              <div> \n              <p>Stok obat awal di outlet ApoteX berjumlah + 2.500 item, dengan nilai + Rp 250 juta.</p>\n              <p>Untuk memastikan kelengkapan obat, PT. PFI memiliki ready-stock obat di luar jenis obat yang dimiliki outlet, \n                 sebanyak + 2.500 item lagi.</p>\n              <p>Berdasarkan data online, PT. PFI mengetahui penjualan dan posisi stok obat di setiap outlet ApoteX.</p>\n              <p>Pembelian dilakukan oleh PT. PFI ke semua rekanan PBF (Pedagang Besar Farmasi) di kota tersebut, \n                 dengan sistem lelang mingguan.</p>\n              <p>Pengiriman obat dari PBF ditujukan ke Distribution Center (DC/gudang) PT. PFI.</p> \n              <p>Selanjutnya DC akan mengirim kebutuhan obat masing-masing outlet, yang dilakukan setiap hari. \n                 Hal ini menyebabkan outlet tidak perlu memiliki level stok obat yang terlalu tinggi.</p>\n              <p>Kota yang belum ada DC diharuskan melakukan stocking di atas standar.</p> \n              </div><br>', 1, NULL, NULL),
(2, 'Discount', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam \nnonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat \nvolutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation \nullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. \nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse \nmolestie consequat, vel illum dolore eu feugiat nulla facilisis at vero \neros et accumsan et iusto odio dignissim qui blandit praesent luptatum \nzzril delenit augue duis dolore te feugait nulla facilisi. Nam liber \ntempor cum soluta nobis eleifend option congue nihil imperdiet doming id\n quod mazim placerat facer possim assum. Typi non habent claritatem \ninsitam; est usus legentis in iis qui facit eorum claritatem. \nInvestigationes demonstraverunt lectores legere me lius quod ii legunt \nsaepius. Claritas est etiam processus dynamicus, qui sequitur mutationem\n consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc\n putamus parum claram, anteposuerit litterarum formas humanitatis per \nseacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis \nvidentur parum clari, fiant sollemnes in futurum.<br>', 1, NULL, NULL),
(3, 'Info Franchise', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam \nnonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat \nvolutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation \nullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. \nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse \nmolestie consequat, vel illum dolore eu feugiat nulla facilisis at vero \neros et accumsan et iusto odio dignissim qui blandit praesent luptatum \nzzril delenit augue duis dolore te feugait nulla facilisi. Nam liber \ntempor cum soluta nobis eleifend option congue nihil imperdiet doming id\n quod mazim placerat facer possim assum. Typi non habent claritatem \ninsitam; est usus legentis in iis qui facit eorum claritatem. \nInvestigationes demonstraverunt lectores legere me lius quod ii legunt \nsaepius. Claritas est etiam processus dynamicus, qui sequitur mutationem\n consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc\n putamus parum claram, anteposuerit litterarum formas humanitatis per \nseacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis \nvidentur parum clari, fiant sollemnes in futurum.<br>', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
`id` int(11) NOT NULL,
  `title` char(100) DEFAULT NULL,
  `description` char(200) DEFAULT NULL,
  `picture` char(200) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE IF NOT EXISTS `social_media` (
`id` int(11) NOT NULL,
  `description` char(200) DEFAULT NULL,
  `url` char(100) DEFAULT NULL,
  `picture` char(100) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscriber`
--

CREATE TABLE IF NOT EXISTS `subscriber` (
`id` int(11) NOT NULL,
  `name` char(50) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` char(30) DEFAULT NULL,
  `password` char(200) DEFAULT NULL,
  `fullname` char(100) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1432799740, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '127.0.0.1', 'iqbalwaee', '$2y$08$8tkuzXRDOdov2m.fPJ5e3uhXE9Hh/L8ovkhCgKWUrqtaKtAbhHpcu', NULL, 'iardiansyah@global-tech.co.id', NULL, NULL, NULL, NULL, 1432798910, 1432800651, 1, 'Teguh', 'Ardiansyah', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
 ADD PRIMARY KEY (`id_option`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriber`
--
ALTER TABLE `subscriber`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`), ADD KEY `fk_users_groups_users1_idx` (`user_id`), ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subscriber`
--
ALTER TABLE `subscriber`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
